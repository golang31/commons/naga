package naga_test

import (
	"errors"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/golang31/commons/naga"
	"gitlab.com/golang31/commons/naga/mocks"
)

func TestNaga(t *testing.T) {

	entriesOK := []naga.ConfigEntry{
		naga.ConfigEntry{
			VariableName: "example_one",
			Description:  "first entry",
			Shortcut:     "no",
			DefaultValue: 0,
		},
		naga.ConfigEntry{
			VariableName: "example_two",
			Description:  "second entry",
			Shortcut:     "nt",
			DefaultValue: "",
		},
		naga.ConfigEntry{
			VariableName: "example_three",
			Description:  "thrid entry",
			Shortcut:     "nth",
			DefaultValue: false,
		},
	}

	entriesNOK := []naga.ConfigEntry{
		naga.ConfigEntry{
			VariableName: "bad value",
			Description:  "going to fail",
			Shortcut:     "b",
			DefaultValue: nil,
		},
	}

	NoEntries := []naga.ConfigEntry{}

	//Configurator that won't have errors
	mockConfigFlag := &mocks.FlagConfigurator{}

	mockConfigFlag.On("ConfigureFlag", mock.AnythingOfType("ConfigEntry")).Return(nil)

	//Configurator that has an error on the FlagConfigurator

	mockBadFlagConfigurator := &mocks.FlagConfigurator{}

	mockBadFlagConfigurator.On("ConfigureFlag", mock.AnythingOfType("ConfigEntry")).Return(errors.New("FlagConfigurator error"))

	//VariableTypeResolverMock
	mockTypeResolver := &mocks.VariableTypeResolver{}
	errorMockTypeResolver := &mocks.VariableTypeResolver{}

	mockTypeResolver.On("ResolveType", mock.AnythingOfType("int")).Return(naga.TypeInt, nil)
	mockTypeResolver.On("ResolveType", mock.AnythingOfType("string")).Return(naga.TypeString, nil)
	mockTypeResolver.On("ResolveType", mock.AnythingOfType("bool")).Return(naga.TypeBool, nil)
	errorMockTypeResolver.On("ResolveType", nil).Return(naga.TypeNone, errors.New("ResolveType error"))

	configurator := naga.NewConfigurator(mockConfigFlag, mockTypeResolver)
	badTypeResolverConfigurator := naga.NewConfigurator(mockConfigFlag, errorMockTypeResolver)
	badConfigurator := naga.NewConfigurator(mockBadFlagConfigurator, mockTypeResolver)

	t.Run("Debe fallar si falla leyendo el configFile", func(t *testing.T) {
		values, err := configurator.Configure("not_existing_file", entriesOK)
		assert.Nil(t, values)
		assert.Error(t, err)
	})

	t.Run(("Debe retornar valores default si no se entrega valor a una variable a través de yaml/env/flags"), func(t *testing.T) {
		values, err := configurator.Configure("", entriesOK)
		assert.Equal(t, 3, len(values))
		assert.Equal(t, 0, values["example_one"])
		assert.Equal(t, "", values["example_two"])
		assert.Equal(t, false, values["example_three"])
		assert.Nil(t, err)
	})

	t.Run("Debe fallar si falla el FlagConfigurator", func(t *testing.T) {
		values, err := badConfigurator.Configure("", entriesOK)
		assert.Nil(t, values)
		assert.Error(t, err)
	})

	t.Run("Debe fallar el ResolveType al recibir un valor nulo", func(t *testing.T) {
		values, err := badTypeResolverConfigurator.Configure("", entriesNOK)
		assert.Equal(t, nil, values["bad_value"])
		assert.Error(t, err)
	})

	t.Run("Debe fallar si no se entregan valores de entrada", func(t *testing.T) {
		values, err := configurator.Configure("", NoEntries)
		assert.Nil(t, values)
		assert.Error(t, err)
	})

	t.Run("Debe leer las variables desde un config file", func(t *testing.T) {
		values, err := configurator.Configure("config_test", entriesOK)
		assert.Equal(t, 3, len(values))
		assert.Equal(t, 42, values["example_one"])
		assert.Equal(t, "new value!", values["example_two"])
		assert.Equal(t, true, values["example_three"])
		assert.Nil(t, err)
	})

	t.Run("Debe extraer los valores desde variables de entorno", func(t *testing.T) {
		os.Setenv("EXAMPLE_ONE", "1")
		os.Setenv("EXAMPLE_TWO", "value set by env var!")
		os.Setenv("EXAMPLE_THREE", "true")
		values, err := configurator.Configure("", entriesOK)
		assert.Nil(t, err)
		assert.Equal(t, 3, len(values))
		assert.Equal(t, 1, values["example_one"])
		assert.Equal(t, "value set by env var!", values["example_two"])
		assert.Equal(t, true, values["example_three"])

		os.Clearenv()
	})

}
