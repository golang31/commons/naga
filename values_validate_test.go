package naga_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/golang31/commons/naga"
)

func TestValidateNaga(t *testing.T) {
	t.Run("Debe configurar correctamente sin panic", func(t *testing.T) {
		config := []naga.ConfigEntry{
			{
				VariableName: "test_1",
				DefaultValue: "test1",
			},
			{
				VariableName: "test_2",
				DefaultValue: "test2",
			},
		}

		vals := map[string]interface{}{"test_1": "test1", "test_2": "test2"}

		mappa := naga.MapValues(vals, config)
		assert.NotNil(t, mappa)

		os.Clearenv()
	})

	t.Run("Debe assertar al devolver un panic por no existir parametros en el mapa de naga", func(t *testing.T) {
		config := []naga.ConfigEntry{
			{
				VariableName: "test_1",
				DefaultValue: "test1",
			},
			{
				VariableName: "test_2",
				DefaultValue: "test2",
			},
		}

		vals := map[string]interface{}{"test_1": "test1"}

		assert.Panics(t, func() { naga.MapValues(vals, config) })

		os.Clearenv()
	})

	t.Run("Debe tirar panic por una entrada nil en las configuraciones", func(t *testing.T) {

		assert.Panics(t, func() { naga.MapValues(nil, nil) })

		os.Clearenv()
	})

	t.Run("Debe traer correctamente la configruacion base de la api", func(t *testing.T) {

		vals := map[string]interface{}{"port": ":8080", "tracing_enabled": false, "logging_level": "info", "metrics_enabled": false, "timeout": 30, "uri_prefix": "/fifco/v1"}

		base := naga.GetBaseCfg(vals)
		assert.NotNil(t, base)
		os.Clearenv()
	})
}
