package naga_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/golang31/commons/naga"
	"gitlab.com/golang31/commons/naga/mocks"
)

func TestFlagConfigurator(t *testing.T) {

	configEntryOKINT := naga.ConfigEntry{
		VariableName: "flagname1",
		Description:  "flagDescription",
		Shortcut:     "a",
		DefaultValue: 1,
	}
	configEntryNOKINT := naga.ConfigEntry{
		VariableName: "flagname1",
		Description:  "",
		Shortcut:     "a",
		DefaultValue: 1,
	}
	configEntryOKBOOL := naga.ConfigEntry{
		VariableName: "flagname2",
		Description:  "flagDescription",
		Shortcut:     "b",
		DefaultValue: true,
	}
	configEntryNOKBOOL := naga.ConfigEntry{
		VariableName: "flagname2",
		Description:  "",
		Shortcut:     "b",
		DefaultValue: true,
	}

	configEntryOKSTRING := naga.ConfigEntry{
		VariableName: "flagname3",
		Description:  "flagDescription",
		Shortcut:     "c",
		DefaultValue: "asdasda",
	}

	configEntryNOKSTRING := naga.ConfigEntry{
		VariableName: "flagname4",
		Description:  "",
		Shortcut:     "d",
		DefaultValue: "STRING",
	}

	configEntryNOK := naga.ConfigEntry{
		VariableName: "",
		Description:  "",
		Shortcut:     "a",
		DefaultValue: "",
	}
	configEntryFLOAT := naga.ConfigEntry{
		VariableName: "floatflag",
		Description:  "asdasd",
		Shortcut:     "f",
		DefaultValue: 2.3,
	}

	mockTypeResolver := &mocks.VariableTypeResolver{}

	flagConfigurator := naga.NewFlagConfigurator(mockTypeResolver)

	mockTypeResolver.On("ResolveType", configEntryNOK.DefaultValue).Return(nil, errors.New("Error en el servicio Type")).
		On("ResolveType", configEntryOKINT.DefaultValue).Return(naga.TypeInt, nil).
		On("ResolveType", configEntryNOKINT.DefaultValue).Return(naga.TypeInt, errors.New(naga.ErrorType)).
		On("ResolveType", configEntryOKBOOL.DefaultValue).Return(naga.TypeBool, nil).
		On("ResolveType", configEntryNOKBOOL.DefaultValue).Return(naga.TypeBool, errors.New(naga.ErrorType)).
		On("ResolveType", configEntryOKSTRING.DefaultValue).Return(naga.TypeString, nil).
		On("ResolveType", configEntryNOKSTRING.DefaultValue).Return(naga.TypeString, errors.New(naga.ErrorType)).
		On("ResolveType", configEntryFLOAT.DefaultValue).Return(naga.TypeNone, errors.New("Error con tipo de dato default"))

	t.Run("Debe fallar si la estructura de configuracion tiene sus campos vacios", func(t *testing.T) {
		err := flagConfigurator.ConfigureFlag(configEntryNOK)
		assert.Error(t, err)
	})
	t.Run("Debe fallar si no puede convertir un entero", func(t *testing.T) {
		err := flagConfigurator.ConfigureFlag(configEntryOKINT)
		assert.Nil(t, err)
	})
	t.Run("Debe fallar si no puede convertir un string", func(t *testing.T) {
		err := flagConfigurator.ConfigureFlag(configEntryOKSTRING)
		assert.Nil(t, err)
	})
	t.Run("Debe fallar si no puede convertir un bool", func(t *testing.T) {
		err := flagConfigurator.ConfigureFlag(configEntryOKBOOL)
		assert.Nil(t, err)
	})

	t.Run("Debe fallar si el Value es de tipo invalido", func(t *testing.T) {
		err := flagConfigurator.ConfigureFlag(configEntryFLOAT)
		assert.Error(t, err)
	})

}
