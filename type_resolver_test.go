package naga_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/golang31/commons/naga"
)

func TestType(t *testing.T) {

	t.Run(("Debe fallar si entra una interface nil"), func(t *testing.T) {
		typeData := naga.NewVariableTypeResolver() //interface
		value, err := typeData.ResolveType(nil)
		assert.Equal(t, naga.TypeNone, value)
		assert.Error(t, err)
	})

	t.Run(("Debe pasar si entra un string"), func(t *testing.T) {
		typeData := naga.NewVariableTypeResolver()
		value, err := typeData.ResolveType("1 2 3 4")
		assert.Equal(t, naga.TypeString, value)
		assert.Nil(t, err)
	})

	t.Run(("Debe pasar si entra un bool"), func(t *testing.T) {
		typeData := naga.NewVariableTypeResolver()
		value, err := typeData.ResolveType(false)
		assert.Equal(t, naga.TypeBool, value)
		assert.Nil(t, err)
	})

	t.Run(("Debe pasar si entra un int"), func(t *testing.T) {
		typeData := naga.NewVariableTypeResolver()
		value, err := typeData.ResolveType(34)
		assert.Equal(t, naga.TypeInt, value)
		assert.Nil(t, err)
	})

	t.Run(("Debe pasar si entra un dato invalido (TypeNone)"), func(t *testing.T) {
		typeData := naga.NewVariableTypeResolver()
		value, err := typeData.ResolveType(2.34)
		assert.Equal(t, naga.TypeNone, value)
		assert.Error(t, err)
	})

}
